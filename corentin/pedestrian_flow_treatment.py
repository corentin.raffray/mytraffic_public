import pandas as pd
import matplotlib.pyplot as plt
from shapely.geometry import Point, Polygon


def creer_dico_pieton():
    df = pd.read_csv('DATA/pedestrian_flow_sample_2023_01_01.csv')

    dico_flux_pieton = {}

    # Parcourir chaque ligne du dataframe
    for index, row in df.iterrows():

        # Extraire les valeurs de la ligne
        lon, lat, valeur = row['lon'], row['lat'], row['value']

        # Créer un tuple clé pour les coordonnées
        key = Point(lon, lat)

        # Vérifier si la clé existe déjà dans le dictionnaire
        if key in dico_flux_pieton:
            # Si la clé existe, ajouter la valeur à la liste existante
            dico_flux_pieton[key].append(valeur)
        else:
            # Si la clé n'existe pas, créer une nouvelle liste avec la valeur
            dico_flux_pieton[key] = valeur
    return dico_flux_pieton


if __name__ == '__main__':
    print(creer_dico_pieton())
