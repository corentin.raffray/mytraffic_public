import pandas as pd
import matplotlib.pyplot as plt


def create_dict():
    df = pd.read_csv('DATA/polygon_flow_sample_2023_01_01.csv')
    # Création d'un DataFrame filtré pour la condition "flow_kind=without_passing_cars"
    filtered_df = df[df['flow_kind'] == 'without_passing_cars']

    # Création du dictionnaire avec les polygon_id et les adjusted_nb_unique_ids correspondants
    result_dict = {}
    for index, row in filtered_df.iterrows():
        result_dict[row['polygon_id']] = row['adjusted_nb_unique_ids']

    return result_dict

# print(create_dict())
