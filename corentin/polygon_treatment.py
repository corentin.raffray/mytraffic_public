import pandas as pd
import matplotlib.pyplot as plt
from shapely.geometry import Polygon, Point
from shapely import BufferCapStyle, BufferJoinStyle

example_key = '0251e6db-57c9-4679-b8a6-35f7e01a7afb'
example_buffer_value = 0.0005


def dicos_polygones():

    # Lire le fichier CSV dans un DataFrame
    df = pd.read_csv('DATA/polygon_flow_sample_2023_01_01.csv')

    # Initialiser un dictionnaire pour stocker les coordonnées des polygones
    polygons_dict = {}

    # Parcourir les lignes du DataFrame
    for index, row in df.iterrows():

        # Extraire l'identifiant du polygone
        polygon_id = row['polygon_id']

        # Extraire les coordonnées des sommets du polygone
        polygon_string = row['geometry_4326']
        # Supprimer les chaînes de caractères inutiles
        polygon_string = polygon_string.replace(')))', '')
        polygon_string = polygon_string.replace('MULTIPOLYGON (((', '')

        polygon_coords = [tuple(map(float, x.split()))
                          for x in polygon_string.split(',')]

        # Stocker le polygone dans le dictionnaire avec son identifiant comme clé et ses coordonnées comme valeur
        polygons_dict[polygon_id] = polygon_coords

    polygons_dict_shapely = {}
    polygon_area = {}

    for key in polygons_dict.keys():
        polygons_dict_shapely[key] = Polygon(polygons_dict[key])
        polygon_area[key] = polygons_dict_shapely[key].area

    return polygons_dict_shapely, polygon_area


def polygon_center(polygon_dict):
    polygon_center = {}
    polygons_dict_shapely = polygon_dict
    for polygone in polygons_dict_shapely.keys():
        polygon_center[polygone] = polygons_dict_shapely[polygone].centroid
    return (polygon_center)


def point_loin(dico_polygones):
    polygon_center_points = polygon_center(dico_polygones)
    point_le_plus_loin = {}
    for id, poly in dico_polygones.items():
        center = polygon_center_points[id]
        max_distance = 0
        farthest_point = None
        for point in poly.exterior.coords:
            distance = center.distance(Point(point))
            if distance > max_distance:
                max_distance = distance
                farthest_point = point
        point_le_plus_loin[id] = [Point(farthest_point), distance]
    return point_le_plus_loin


def upgrade_polygons(buffer_value):
    polygones_dict = dicos_polygones()[0]
    for polygone in polygones_dict.keys():
        polygones_dict[polygone] = polygones_dict[polygone].buffer(
            buffer_value)
    return polygones_dict


def affichage_polygones():
    polygons_dict_shapely = dicos_polygones()[0]
    polygon_center_point = polygon_center(polygons_dict_shapely)
    point_le_plus_loin = point_loin(polygons_dict_shapely)
    for polygone in polygons_dict_shapely.keys():
        polygone_a_dessiner = polygons_dict_shapely[polygone]
        polygon_upgraded = polygone_a_dessiner.buffer(example_buffer_value)
        point_grav = polygon_center_point[str(polygone)]
        farthest_point = point_le_plus_loin[polygone][0]

        fig, ax = plt.subplots()
        x0, y0 = polygone_a_dessiner.exterior.xy
        x, y = polygon_upgraded.exterior.xy

        ax.fill(x, y, alpha=0.5, fc='blue', ec='black')
        ax.fill(x0, y0, fc='red', ec='black')

        ax.scatter(point_grav.x, point_grav.y, color='black')
        ax.scatter(farthest_point.x, farthest_point.y, color='green')

        plt.show()


if __name__ == "__main__":
    affichage_polygones()
