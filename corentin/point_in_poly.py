from .pedestrian_flow_treatment import creer_dico_pieton
from .polygon_treatment import dicos_polygones, polygon_center, point_loin, upgrade_polygons
from shapely.geometry import Point, Polygon
import matplotlib.pyplot as plt
from math import sqrt

polygon_area = dicos_polygones()[1]
polygon_dict = upgrade_polygons(0.0005)
dico_pieton = creer_dico_pieton()
polygon_center_points = polygon_center(polygon_dict)
point_le_plus_loin = point_loin(polygon_dict)


def distance_calc(point_1, point_2):
    # On passe du format shapely au tuple
    point1 = (point_1.x, point_1.y)
    point2 = (point_2.x, point_2.y)
    return sqrt((point1[0]-point2[0])**2 + (point1[1] - point2[1])**2)


def methode_un_peu_moins_naive():
    flux_poly_estime = {}

    for polygon in polygon_dict.keys():
        center = polygon_center_points[polygon]
        coord_point_loin = point_le_plus_loin[polygon][0]
        d_max = center.distance(coord_point_loin)

        for point in dico_pieton.keys():
            if dicos_polygones()[0][polygon].contains(point):
                distance = center.distance(point)
                coefficient = (1 - (distance / d_max))

                if polygon in flux_poly_estime.keys():
                    flux_poly_estime[polygon] += float(coefficient) * \
                        float(dico_pieton[point])
                else:
                    flux_poly_estime[polygon] = float(
                        coefficient) * float(dico_pieton[point])
    return flux_poly_estime


if __name__ == "__main__":
    print(methode_un_peu_moins_naive())
