import numpy as np
import pandas as pd

from corentin.polygon_flow_treatment import create_dict
from corentin import polygon_flow_treatment
from corentin.point_in_poly import *

A=flux_poly_estime
B=A.values()
X=np.array(B)
y=np.array(create_dict().values())

X=np.array()
y=np.array()

def linear_regression(X, y, alpha=0.01, num_iterations=1000):
    """
    Effectue une régression linéaire à l'aide de la descente de gradient.
    
    Parameters:
    X (numpy array): un tableau de dimensions (m, n), où m est le nombre d'exemples et n est le nombre de caractéristiques
    y (numpy array): un tableau de dimensions (m,), où m est le nombre d'exemples
    alpha (float): taux d'apprentissage (par défaut = 0.01)
    num_iterations (int): nombre d'itérations de la descente de gradient (par défaut = 1000)
    
    Returns:
    theta (numpy array): un tableau de dimensions (n,), où n est le nombre de caractéristiques. Les valeurs de theta sont les coefficients de la régression linéaire.
    """
    
    m, n = X.shape
    theta = np.zeros(n)
    
    for i in range(num_iterations):
        h = np.dot(X, theta)
        loss = h - y
        gradient = np.dot(X.T, loss) / m
        theta -= alpha * gradient
        
    return theta 

# Theta est un vecteur colonne de dimensions (p+1) x 1, où p est le nombre de variables explicatives
theta = ...

# X est une matrice de dimensions n x (p+1), où chaque ligne correspond à un exemple dans votre jeu de données et la dernière colonne contient les flux estimés
X = ...

# y est un vecteur colonne de dimensions n x 1, contenant les flux réels correspondant à chaque exemple dans votre jeu de données
y = ...

# Calculer les flux estimés pour chaque exemple dans X en utilisant les paramètres theta
flux_estimes = X.dot(theta)

# Calculer la différence entre les flux réels et estimés
differences = y - flux_estimes




