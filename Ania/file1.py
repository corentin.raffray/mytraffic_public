import pandas as pd
import folium
from folium.plugins import MarkerCluster

# Lecture du CSV dans un dataframe pandas
df = pd.read_csv("Data/pedestrian_flow_sample_2023_01_01.csv")

# Création d'une carte centrée sur la France
map = folium.Map(location=[46.2276, 2.2137], zoom_start=6)

# Création de marqueurs
marker_cluster = MarkerCluster().add_to(map)

for index, row in df.iterrows():
    #latitude et la longitude
    if "-" in row["code"]:
        lat, lon = row["code"].rsplit("-", 1)
        try:
            lat = float(lat)
            lon = float(lon)
        except ValueError:
            continue  
    else:
        continue 

    # Création d'un popup
    popup_text = f"Valeur : {row['value']}, Date : {row['month']}-{row['day']}"
    popup = folium.Popup(popup_text, max_width=300)

    # Ajouter des regroupements de marqueurs
    folium.Marker(location=[lon, lat], popup=popup).add_to(marker_cluster)

# Affichage de la carte
map.save("map.html")
