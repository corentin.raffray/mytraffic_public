import pandas as pd
from scipy.spatial import Voronoi, voronoi_plot_2d
import folium
import matplotlib.pyplot as plt
from folium.plugins import MarkerCluster

# Charger les données dans un dataframe Pandas
df = pd.read_csv("Data/pedestrian_flow_sample_2023_01_01.csv")

# Obtenir les coordonnées des points de données
points = df[['lat', 'lon']].values

# Calculer le diagramme de Voronoi
vor = Voronoi(points)

# Visualiser le diagramme de Voronoi sur une carte
m = folium.Map(location=[46.2276, 2.2137], zoom_start=6)

for i, region in enumerate(vor.regions):
    if not region:
        continue
    color = '#%06x' % (i * 123456 % 0xffffff)
    vertices = [vor.vertices[i] for i in region]
    if vertices:
        folium.Polygon(locations=vertices, color=color, fill_color=color, fill_opacity=0.4).add_to(m)

# Création de marqueurs
marker_cluster = MarkerCluster().add_to(m)

for index, row in df.iterrows():
    #latitude et la longitude
    if "-" in row["code"]:
        lat, lon = row["code"].rsplit("-", 1)
        try:
            lat = float(lat)
            lon = float(lon)
        except ValueError:
            continue  
    else:
        continue 

    # Création d'un popup
    popup_text = f"Valeur : {row['value']}, Date : {row['month']}-{row['day']}"
    popup = folium.Popup(popup_text, max_width=300)

    # Ajouter des regroupements de marqueurs
    folium.Marker(location=[lon, lat], popup=popup).add_to(marker_cluster)

# Enregistrer et afficher la carte
m.save('voronoi2_map.html')
